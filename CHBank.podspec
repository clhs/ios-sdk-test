Pod::Spec.new do |spec|
  spec.swift_version = '5.1'
  spec.name         = "CHBank"
  spec.version      = "0.1.78"
  spec.summary      = "CLHMBank Module"
  spec.description  = <<-DESC
  CLHMBank module
                   DESC

  spec.homepage     = "https://www.clhs.com.ua/"
  spec.license      = "MIT"
  # spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  spec.author             = { "clhs" => "info@clhs.com.ua" }
  spec.ios.deployment_target = '10.2'
  spec.platform = :ios, '10.2'
#  spec.ios.vendored_frameworks = 'CHBank.xcframework'
  spec.ios.vendored_frameworks = 'CHBank.framework'
#spec.vendored_frameworks = "GoogleMaps.framework"
#  spec.source            = { :http => 'https://bitbucket.org/clhs/ios-sdk-test/raw/8459be46d86534aa2a921cdbf1cb1236fbd93734/CHBank.xcframework.zip' }
  spec.source            = { :http => 'https://bitbucket.org/clhs/ios-sdk-test/raw/a06c85323f19c9ac13d30090625c9f15557f8e4c/CHBankSDK.zip' }

#spec.source_files = 'CHBank/*{h,m,swift}', 'CHBank/**/*.{h,m,swift}'
#  spec.resources = 'CHBank/Resources/**/*', 'CHBank/**/*.{xib,strings}'

# spec.frameworks = "UIKit"
  spec.frameworks = "UIKit", "AVFoundation"
#spec.dependency "GoogleMaps"
#  spec.dependency "Google-Maps-iOS-Utils"
#spec.dependency "GoogleMLKit/FaceDetection"
#spec.dependency "Firebase/MLVision", "6.25.0"
#  spec.dependency "Firebase/MLVisionFaceModel", "6.25.0"
  spec.dependency "Firebase", "6.25.0"
#spec.static_framework = true 
#spec.xcconfig = { 'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES'}
#  spec.pod_target_xcconfig = {
#    "OTHER_LDFLAGS" => '$(inherited) -framework "Firebase" -framework "FirebaseMLVision" -framework "FirebaseMLVisionFaceModel"',
#    "CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES" => "YES",
#    "FRAMEWORK_SEARCH_PATHS" => '$(inherited)../../',
#  }

end
