#!/bin/bash

if [ ! $TAG ]; then
        read -p "write new tag: " TAG
fi

git add .
git commit -m "$TAG"
git push
git tag $TAG -a -m "$TAG"
git push --tags
